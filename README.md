# Broadcast (Turtle) #

This project is a variation on the Broadcast AR demo. The Broadcast AR Demo uses a TV (or other large screen) as a large Vuforia AR marker.  

This demo has a sea turtle swim out of the Marker on-tap. Then the turtle swims around the user's head using a combo IMU/AR to simulate a SLAM style tracking (moving breaks the experience).

### Info ###

* Engine: Unity 2017.3.0f3
* Platform: iOS/mobile
* Frameworks: Vuforia 7.x
  * Uses the included version from >= Unity 2017.2
* Tracker Images
  * NatGeo Logo


## Who do I talk to? ##

* Cameron E - cameron@yeticgi.com
* John C - john@yeticgi.com
* Dillon W - dillon@yeticgi.com


# Changelog #

### 1.0.0 [2016/03/29]
Added
* Uses NatGeo tracker image
* turtle swims around